package com.company.service.output;

public class ConsoleOutput implements Output{
    public void print(String text) {
        System.out.println(text);
    }
}
