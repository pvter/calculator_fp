package com.company.service.input;

import java.io.IOException;

public interface Input {
    public String read() throws IOException;
}
