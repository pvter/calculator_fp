package com.company.exception;

public class ExtraBracketException extends Exception {
    @Override
    public String getMessage() {
        return "В выражении лишняя скобка.";
    }
}
