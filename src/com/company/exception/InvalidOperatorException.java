package com.company.exception;

public class InvalidOperatorException extends Exception {
    Character operator;

    public InvalidOperatorException(Character operator) {
        this.operator = operator;
    }

    @Override
    public String getMessage() {
        return "недопустимый оператор: " + this.operator;
    }
}
