package com.company.exception;

public class EmptyExpressionException extends Exception {
    @Override
    public String getMessage() {
        return "Пустое выражение.";
    }
}
