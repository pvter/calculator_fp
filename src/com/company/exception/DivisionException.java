package com.company.exception;

public class DivisionException extends Exception {
    @Override
    public String getMessage() {
        return "Недопустимый делитель.";
    }
}
