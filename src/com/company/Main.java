package com.company;

import com.company.calculator.Calculator;
import com.company.calculator.DijkstraCalculator.DijkstraCalculator;
import com.company.service.errorHandler.ErrorHandler;
import com.company.service.input.ConsoleInput;
import com.company.service.input.Input;
import com.company.service.output.ConsoleOutput;
import com.company.service.output.Output;

import static com.company.util.StringUtil.formatResult;

public class Main {
    private static final Input input = new ConsoleInput();
    private static final Output output = new ConsoleOutput();
    private static final ErrorHandler errorHandler = new ErrorHandler(output);
    private static final Calculator calculator = new DijkstraCalculator();

    public static void main(String[] args) {
        try {
            final String inputExpression = input.read();
            final double result = calculator.calculate(inputExpression);
            final String formattedResult = formatResult(result);
            output.print("Результат вычисления: \n" + formattedResult);
        } catch (Exception e) {
            errorHandler.handleError(e);
        }

        output.print("\n");
        main(args);
    }
}
