package com.company.util;

import java.util.Arrays;
import java.util.List;

public class CharUtil {
    public static boolean isDigit(char ch) {
        return Character.isDigit(ch);
    }

    public static boolean isOperator(char ch) {
        List<Character> operators = Arrays.asList('+', '-', '−', '*', '/');
        return operators.contains(ch);
    }
}
