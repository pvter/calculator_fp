package com.company.calculator.DijkstraCalculator;

import com.company.calculator.Calculator;
import com.company.exception.EmptyExpressionException;
import com.company.exception.InvalidExpressionException;
import com.company.validator.CalculatorValidator;
import com.company.validator.DijkstraValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.company.util.CharUtil.isOperator;
import static com.company.util.StringUtil.isNumber;

public class DijkstraCalculator extends Calculator {
    private final CalculatorValidator validator = new DijkstraValidator();

    public double calculate(String expression) throws Exception {
        validator.validate(expression);
        final String RPNExpression = RPNConverter.convert(expression);
        final List<String> items = stringToList(RPNExpression);
        return calcResult(items);
    }

    private double calcResult(List<String> items) throws Exception {
        if (items.isEmpty()) throw new EmptyExpressionException();

        if (items.size() == 1) {
            if (!isNumber(items.get(0))) throw new InvalidExpressionException();
            return Double.parseDouble(items.get(0));
        }

        final OptionalInt optPos = getFirstOperatorIndex(items);
        if (optPos.isEmpty()) return calcResult(items);

        final int operatorPosition = optPos.getAsInt();
        if (operatorPosition == 0 || items.get(operatorPosition - 1).isEmpty())
            throw new InvalidExpressionException();

        final char o = items.get(operatorPosition).charAt(0);
        final double y = Double.parseDouble(items.get(operatorPosition - 1));
        final double x = operatorPosition == 1 ? 0 : Double.parseDouble(items.get(operatorPosition - 2));
        final double result = performOperation(o, x, y);

        if (o == '−') {
            items.set(operatorPosition - 1, Double.toString(result));
            items.remove(operatorPosition);
            return calcResult(items);
        }

        if (operatorPosition > 1) {
            items.set(operatorPosition - 2, Double.toString(result));
            items.remove(operatorPosition);
            items.remove(operatorPosition - 1);
        } else {
            items.set(0, Double.toString(result));
            items.remove(operatorPosition);
        }
        return calcResult(items);
    }

    private List<String> stringToList(String expression) {
        return new ArrayList<>(List.of(expression.split(" ")))
                .stream()
                .filter(item -> item.length() > 0)
                .collect(Collectors.toList());
    }

    private OptionalInt getFirstOperatorIndex(List<String> items) {
        return IntStream.range(0, items.size())
                .filter(i -> stackItemIsOperator(items.get(i)))
                .findFirst();
    }

    private boolean stackItemIsOperator(String item) {
        return item.length() == 1 && isOperator(item.charAt(0));
    }

}
