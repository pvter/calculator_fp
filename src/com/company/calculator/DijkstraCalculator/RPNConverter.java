package com.company.calculator.DijkstraCalculator;

import com.company.exception.ExtraBracketException;
import com.company.exception.InvalidExpressionException;
import com.company.exception.InvalidOperatorException;
import com.company.service.output.ConsoleOutput;
import com.company.service.output.Output;

import java.util.List;
import java.util.Stack;

import static com.company.util.CharUtil.isDigit;
import static com.company.util.CharUtil.isOperator;

public class RPNConverter {
    private final static Output output = new ConsoleOutput();

    /**
     * Преобразование выражения в формат обратной польской нотации
     */
    public static String convert(String expression) throws Exception {
        final String result = convertRecursively(expression, "", new Stack<>());
        printOutput(result);
        return result;
    }

    private static String convertRecursively(String input, String result, Stack<Character> stack) throws Exception {
        // Если во входной строке больше не осталось лексем
        if (input.isEmpty()) return convertRecursively(result, stack);
        final char currentChar = input.charAt(0);
        if (isDigit(currentChar) || currentChar == '.') return processDigit(input, result, stack);
        if (currentChar == '(') return processOpeningParenthesis(input, result, stack);
        if (currentChar == ')') return processClosingParenthesis(input, result, stack);
        if (isOperator(currentChar)) return processOperator(input, result, stack);
        throw new InvalidExpressionException();
    }

    private static String convertRecursively(String result, Stack<Character> stack) throws Exception {
        final StringBuilder newResult = new StringBuilder(result);
        if (stack.empty()) return String.valueOf(newResult).trim();
        // если есть оператор в стеке - перекладываем оператор из стека в выходную очередь.
        final char lastItem = stack.pop();
        // Если на вершине стека скобка — в выражении допущена ошибка.
        if (lastItem == '(' || lastItem == ')') throw new ExtraBracketException();
        newResult.append(' ').append(lastItem);
        return convertRecursively(String.valueOf(newResult), stack);
    }

    private static char getFirstChar(String input, Stack<Character> stack) {
        final char firstChar = input.charAt(0);
        // кодируем унарный минус отдельным символом
        if (firstChar == '-' && !stack.isEmpty() && stack.peek() == '(') return '−';
        return firstChar;
    }

    // Если лексема — число, добавляем в строку вывода.
    private static String processDigit(String input, String result, Stack<Character> stack) throws Exception {
        return convertRecursively(input.substring(1), result + input.charAt(0), stack);
    }

    // Если лексема — открывающая скобка, помещаем в стек.
    private static String processOpeningParenthesis(String input, String result, Stack<Character> stack) throws Exception {
        stack.push(input.charAt(0));
        return convertRecursively(input.substring(1), result, stack);
    }

    // Если лексема — закрывающая скобка
    private static String processClosingParenthesis(String input, String result, Stack<Character> stack) throws Exception {
        final StringBuilder newResult = new StringBuilder(result);
        // Пока лексема на вершине стека не станет открывающей скобкой, перекладываем лексемы-операторы из стека в выходную очередь.
        while (!stack.isEmpty() && stack.peek() != '(') newResult.append(' ').append(stack.pop());
        if (!stack.isEmpty() && stack.peek() == '(') stack.pop();// Удаляем из стека открывающую скобку.
        else
            throw new ExtraBracketException(); // Если стек закончился до того, встретилась открывающая скобка — в выражении содержится ошибка.
        return convertRecursively(input.substring(1), String.valueOf(newResult), stack);
    }

    // Если лексема — Оператор O1.
    private static String processOperator(String input, String result, Stack<Character> stack) throws Exception {
        final StringBuilder newResult = new StringBuilder(result);
        final char currentChar = getFirstChar(input, stack);
        final int currentCharPriority = getPriority(currentChar);

        // Пока присутствует на вершине стека лексема-оператор (O2), чей приоритет выше или равен приоритету O1,
        // перекладываем O2 из стека в выходную очередь.
        if (!stack.isEmpty() && isOperator(stack.peek())) {
            final char lastStackItem = stack.peek();
            final int lastStackItemPriority = getPriority(lastStackItem);
            if (lastStackItemPriority >= currentCharPriority) {
                newResult.append(' ').append(stack.pop());
                return convertRecursively(input, String.valueOf(newResult), stack);
            }
        }

        // Помещаем O1 в стек.
        stack.push(currentChar);
        newResult.append(' ');

        return convertRecursively(input.substring(1), String.valueOf(newResult), stack);
    }

    /**
     * Получение приоритета оператора
     */
    private static int getPriority(char c) throws Exception {
        if (List.of('*', '/').contains(c)) return 1;
        if (List.of('+', '-', '−').contains(c)) return 0;
        throw new InvalidOperatorException(c);
    }

    /**
     * Вывод выражения в консоль
     */
    private static void printOutput(String outputExpression) {
        output.print("Выражение в обратной польской нотации:");
        output.print(outputExpression);
    }
}
