package com.company.validator;

public interface CharValidator {
    void validate(Character currentChar, Character prevChar, Character nextChar) throws Exception;
}
