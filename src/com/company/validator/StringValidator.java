package com.company.validator;

public interface StringValidator {
    void validate(String str) throws Exception;
}
