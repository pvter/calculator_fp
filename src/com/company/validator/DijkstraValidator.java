package com.company.validator;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DijkstraValidator implements CalculatorValidator {
    private final List<StringValidator> stringValidators = List.of(
            new EmptyStringValidator(),
            new InvalidCharValidator(Arrays.asList('.', '(', ')'))
    );

    private final List<CharValidator> charValidators = List.of(
            new DelimiterValidator(),
            new OpeningParenthesisValidator(),
            new ClosingParenthesisValidator(),
            new OperatorValidator()
    );

    /**
     * Проверка арифметического выражения на валидность
     */
    public void validate(String expression) throws Exception {
        stringValidators.forEach(validator -> {
            try {
                validator.validate(expression);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        });

        AtomicInteger i = new AtomicInteger();
        Stream.of(expression.split("")).forEach(ch -> {
            int currentIdx = i.get();
            char currentChar = ch.charAt(0);
            Character prevChar = null;
            Character nextChar = null;

            if (currentIdx > 0) prevChar = expression.charAt(currentIdx - 1);
            if (currentIdx < expression.length() - 1) nextChar = expression.charAt(currentIdx + 1);

            try {
                validateChar(currentChar, prevChar, nextChar);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }

            i.incrementAndGet();
        });
    }

    private void validateChar(Character currentChar, Character prevChar, Character nextChar) throws Exception {
        charValidators.forEach(validator -> {
            try {
                validator.validate(currentChar, prevChar, nextChar);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        });
    }
}
